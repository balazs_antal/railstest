Rails.application.routes.draw do
  get 'widget_order/index'
  root 'widget_order#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
