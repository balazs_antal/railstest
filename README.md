# README

Billing System For Widget Sales:
    - Generates 100 random widget orders for the current month
    - Report Generator generates 3 different sales reports and returns it in JSON format
    - Tests are using RSpec
    - Test data is in the seeds.rb file for both testing and development