class CreatePricings < ActiveRecord::Migration[5.2]
  def change
    create_table :pricings do |t|
      t.integer :minimum_quantity
      t.integer :price
      t.string :method

      t.timestamps
    end
  end
end
