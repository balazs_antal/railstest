class PricingPartnerChangeMethodToMethodId < ActiveRecord::Migration[5.2]
  def change
  	remove_column :pricings, :method, :string
  	remove_column :partners, :method, :string

  	add_reference :pricings, :sale_method, foreign_key: true
  	add_reference :partners, :sale_method, foreign_key: true
  end
end
