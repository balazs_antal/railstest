class CreateWidgetOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :widget_orders do |t|
      t.integer :quantity
      t.integer :price
      t.datetime :date

      t.timestamps
    end
  end
end
