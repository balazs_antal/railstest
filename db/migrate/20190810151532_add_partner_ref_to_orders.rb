class AddPartnerRefToOrders < ActiveRecord::Migration[5.2]
  def change
	add_reference :widget_orders, :partner, foreign_key: true
  end
end
