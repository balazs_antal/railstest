class CreatePartners < ActiveRecord::Migration[5.2]
  def change
    create_table :partners do |t|
      t.string :name
      t.string :method
      t.integer :sale_price

      t.timestamps
    end
  end
end
