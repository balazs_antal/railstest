# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

SaleMethod.create([{name: "Direct Sale", id: 1}, {name: "Affiliate", id: 2}, {name: "Reseller", id: 3}])

Partner.create([{name: "Direct Sale", sale_method_id: 1, sale_price: 100}, {name: "A Company", sale_method_id: 2, sale_price: 75}, {name: "Another Company", sale_method_id: 2, sale_price: 65}, {name: "Even More Company", sale_method_id: 2, sale_price: 80}, {name: "Resell This", sale_method_id: 3, sale_price: 75}, {name: "Sell More Things", sale_method_id: 3, sale_price: 85}])

Pricing.create([{sale_method_id: 1, price: 100, minimum_quantity: 1}, {sale_method_id: 2, price: 60, minimum_quantity: 1}, {sale_method_id: 2, price: 50, minimum_quantity: 501}, {sale_method_id: 2, price: 40, minimum_quantity: 1001}, {sale_method_id: 3, price: 50, minimum_quantity: 1}])

WidgetOrder.create([{quantity: 55, price: 75, partner_id: 1}, {quantity: 15, price: 75, partner_id: 1}])