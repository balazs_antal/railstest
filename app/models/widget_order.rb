##
# Stores the WidgetOrders registered in the application
# A WidgetOrder belongs to a Partner
class WidgetOrder < ApplicationRecord
	belongs_to :partner
	
	validates :partner, presence: true
	validates :quantity, numericality: { only_integer: true, greater_than: 0 }
	validates :price, numericality: { only_integer: true, greater_than: 0 }
end
