##
# Defines the SaleMethods
# SaleMethod connects to Partner and Pricing
class SaleMethod < ApplicationRecord
	has_many :partners
	has_many :pricings

	validates :name, presence: true	
end
