##
# Defines the pricing for each SaleMethod
# Pricing belongs to a SaleMethod
class Pricing < ApplicationRecord
	belongs_to :sale_method

	validates :sale_method_id, numericality: { only_integer: true, greater_than: 0 }
	validates :price, numericality: { only_integer: true, greater_than: 0 }
	validates :minimum_quantity, numericality: { only_integer: true, greater_than: 0 }
end
