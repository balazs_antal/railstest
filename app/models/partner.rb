##
# Defines the partners and relationships of partners stored in the application
# Partners can have many WidgetOrders and they also belong to a SaleMethod
class Partner < ApplicationRecord
	has_many :widget_orders
	belongs_to :sale_method

	validates :name, presence: true
	validates :sale_method_id, numericality: { only_integer: true, greater_than: 0 }
	validates :sale_price, numericality: { only_integer: true, greater_than: 0 }
 
end
