##
# This class is responsible for generating the reports
class ReportGenerator < ApplicationService
  def initialize
  end

  ##
  # Function calls reporting private function
  # Takes one argument and passes it to reporting
  # Valid report_type values:
  # totalAmount - Generates: Total amount we should bill each Affiliate and Reseller
  # profitEarned - Generates: Profit earned by each Affiliate and Reseller
  # totalRevenue - Generates: Total revenue for the company from all sales: Affiliate, Reseller and Direct 
  def call(report_type)
    return reporting(report_type)
  end

  private
  ##
  # Takes one argument which describes the report type. 
  # It has three valid values: 
  # totalAmount - Generates: Total amount we should bill each Affiliate and Reseller
  # profitEarned - Generates: Profit earned by each Affiliate and Reseller
  # totalRevenue - Generates: Total revenue for the company from all sales: Affiliate, Reseller and Direct
  # Returns JSON
  # On success with the values of the report
  # If the given report type is not supported, an error message 
  def reporting(report_type)
    report = {}
    if report_type.blank?
      report['error'] = "Unknown Report Type"
    else
      if report_type == 'totalRevenue'
        partners = Partner.all.order('sale_method_id ASC')
      else
        partners = Partner.where("sale_method_id > 1")
      end
      
      partners.each do |partner|
        total_order_quantity = partner.widget_orders.sum(:quantity)
        price = 0
        price = partner.sale_method.pricings.where("minimum_quantity <= #{total_order_quantity}").order("minimum_quantity DESC").first.price if !partner.sale_method.pricings.where("minimum_quantity <= #{total_order_quantity}").order("minimum_quantity DESC").first.blank?
        
        case report_type
        when 'totalAmount'
          report[partner.name] = price * total_order_quantity
        when 'profitEarned'
          report[partner.name] = (partner.sale_price - price) * total_order_quantity
        when 'totalRevenue'
          total_price = price * total_order_quantity
          report[partner.sale_method.name] = report[partner.sale_method.name].to_i + total_price
          report["total"] = report["total"].to_i + total_price
        else
          report['error'] = "Unknown Report Type"
        end
      end
    end
    return report.to_json
  end
  
end