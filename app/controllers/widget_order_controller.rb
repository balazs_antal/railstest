##
# This class is responsible for generating the 100 random widget order
# Also this is the only public route in the app. So accessing the homepage by browser also generates the seed.

class WidgetOrderController < ApplicationController
  ##
  # Calls generate_random_seed() function and renders the respons in JSON format
  def index
  	response = generate_random_seed()
    if !response
      render json: {"Error" => "Error During Random Seed Generation"}
    else
      render json: response
    end
  end

  private

  ##
  # Generates a random date for the current month between the first day of the month and today
  def generate_order_date()
  	current_date = Date.current
  	current_date.year.to_s + "-" + current_date.month.to_s + "-" + rand(current_date.beginning_of_month.day..current_date.day).to_s  	
  end

  ##
  # Generates the random seed. Deletes all WidgetOrders from the database than creates 100 new.
  # Quantity between 1-100, picking a random Partner from database and with random order date
  # Returns false if there is no Partner in database or the new WidgetOrder could not be saved.
  # Otherwise returns the created WidgetOrders
  def generate_random_seed()
    WidgetOrder.delete_all
    return false if Partner.count < 1

  	for i in 1..100
  		quantity = rand(1..100)
  		partner = Partner.find(rand(1..Partner.count))
  		partner_id = partner.id
  		order_date = generate_order_date()
  		sale_price = partner.sale_price

  		order = WidgetOrder.create(quantity: quantity, partner_id: partner_id, price: sale_price, date: order_date)
      if !order.valid?
        return false
      end
  	end

    return WidgetOrder.all	
  end
end
