FactoryBot.define do
  factory :partner do
    name { "Test Partner" }
    sale_price  { 100 }
    sale_method
  end

  factory :sale_method do
    name { "Test Method" }    
  end

  factory :pricing do
    price { 100 }
    minimum_quantity { 1 }    
    sale_method
  end

  factory :widget_order do
    quantity { 100 }
    price { 70 }    
    partner
  end
end