require 'rails_helper'

RSpec.describe SaleMethod, :type => :model do
	context 'validations' do
		it { should validate_presence_of(:name)}
		it { should have_many(:partners) }
		it { should have_many(:pricings) }
	end
end