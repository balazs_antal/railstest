require 'rails_helper'

RSpec.describe Pricing, :type => :model do
	context 'validations' do
		it { should validate_numericality_of(:price)}
		it { should validate_numericality_of(:minimum_quantity)}
		it { should belong_to(:sale_method)}
	end
end