require 'rails_helper'

RSpec.describe Partner, :type => :model do
	context 'validations' do
		it { should validate_presence_of(:name)}
		it { should validate_numericality_of(:sale_price)}
		it { should belong_to(:sale_method)}
		it { should have_many(:widget_orders) }
	end
end