require 'rails_helper'

RSpec.describe WidgetOrder, :type => :model do
	context 'validations' do
		it { should validate_numericality_of(:quantity)}
		it { should validate_numericality_of(:price)}
		it { should belong_to(:partner)}
	end
end