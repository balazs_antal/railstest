require 'rails_helper'

RSpec.describe ReportGenerator do
	context 'validations' do
		let(:reportGenerator) { ReportGenerator.new() }
		it "returns error message with empty report type" do
			report = {}
			report['error'] = "Unknown Report Type"
			@expected = report.to_json
  			expect(reportGenerator.call("")).to eq(@expected)
		end

		it "returns error message with unknown report type" do
			FactoryBot.create_list(:widget_order, 100)
			
			report = {}
			report['error'] = "Unknown Report Type"
			@expected = report.to_json
  			expect(reportGenerator.call("RandomReport")).to eq(@expected)
		end

		it "totalAmount returns json with value" do
			FactoryBot.create_list(:widget_order, 100)
			
			response = JSON.parse(reportGenerator.call("totalAmount"))
  			expect(response['Test Partner']).to eq(0)
		end

		it "profitEarned returns json with value" do
			FactoryBot.create_list(:widget_order, 100)
			
			response = JSON.parse(reportGenerator.call("profitEarned"))
  			expect(response['Test Partner']).to eq(10000)
		end

		it "totalRevenue returns json with value" do
			FactoryBot.create_list(:widget_order, 100)
			
			response = JSON.parse(reportGenerator.call("totalRevenue"))
  			expect(response['total']).to eq(0)
		end
	end
end
