require 'rails_helper'

RSpec.describe WidgetOrderController, type: :controller do
	context 'validations' do
		it "returns a successful 200 response in json" do
   			get :index, format: :json
  			expect(response).to be_success
		end

		it "generate random seed with Partners " do
			@unexpected = {"Error" => "Error During Random Seed Generation"}.to_json
			FactoryBot.create_list(:partner, 5)

   			get :index, format: :json
  			expect(response.body).not_to eq(@unexpected)
		end
	end
end
